//
//  AuthInputValidateUtility.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

class AuthInputValidateUtility {
    class func isValidEmail(_ email: String) -> Bool {
        guard !email.characters.isEmpty else {
            return false
        }
        let emailRegex = Constants.InputFieldsConstants.regexEmail
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    class func isValidPassword(_ password: String) -> Bool {
        guard !password.characters.isEmpty else {
            return false
        }
        guard password.characters.count >= Constants.InputFieldsConstants.minPasswordLength && password.characters.count < Constants.InputFieldsConstants.maxPasswordLength else {
            return false
        }
        let passwordRegex = Constants.InputFieldsConstants.regexPassword
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: password)
    }
}
