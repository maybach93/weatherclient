//
//  KeyboardRevealHelper.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

class KeyboardRevealHelper {
    
    //MARK: - Variables
    
    private weak var view: UIView?
    private let defaultContentHeight: CGFloat
    private weak var contentBottomConstraint: NSLayoutConstraint?
    
    //MARK: Lifecycle
    
    init(view: UIView, defaultContentHeight: CGFloat, contentBottomConstraint: NSLayoutConstraint) {
        self.view = view
        self.defaultContentHeight = defaultContentHeight
        self.contentBottomConstraint = contentBottomConstraint
        contentBottomConstraint.constant = defaultContentHeight
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Private methods
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.contentBottomConstraint?.constant = defaultContentHeight
            } else {
                if let endFrame = endFrame?.size.height {
                    self.contentBottomConstraint?.constant = endFrame < defaultContentHeight ? defaultContentHeight : endFrame
                } else {
                    self.contentBottomConstraint?.constant = defaultContentHeight
                }
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view?.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc private func dismissKeyboard() {
        view?.endEditing(true)
    }
}
