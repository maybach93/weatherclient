//
//  LocationProvider.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation
import CoreLocation

class LocationProvider: NSObject {
    
    typealias LocationCompletionClosure = ((CLLocation?) -> Void)?
    
    //MARK: - Variables
    
    static let shared = LocationProvider()
    
    fileprivate lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        return locationManager
    }()
    
    fileprivate var isLocationServicesEnabled: Bool {
        get {
            return CLLocationManager.locationServicesEnabled()
        }
    }
    
    fileprivate var isAuthorized: Bool {
        get {
            let authStatus = CLLocationManager.authorizationStatus()
            return (authStatus == .notDetermined || authStatus == .authorizedAlways || authStatus == .authorizedWhenInUse)
        }
    }
    
    private(set) var lastLocation: CLLocation?
    private var currentLocationCompletion: ((CLLocation?) -> Void)?
    
    //MARK: - Private methods
    
    fileprivate func locationUpdateComplete(location: CLLocation?) {
        
        if let completion = currentLocationCompletion {
            completion(location)
            currentLocationCompletion = nil
        }
        
        if let location = location {
            lastLocation = location
        }
    }
    
    //MARK: - Public methods
    
    public func requestCurrentLocation(_ closure: LocationCompletionClosure) {
        currentLocationCompletion = closure
        
        guard isLocationServicesEnabled else {
            locationUpdateComplete(location: nil)
            return
        }
        
        let authStatus = CLLocationManager.authorizationStatus()
        if !isAuthorized {
            if authStatus == .restricted || authStatus == .denied {
                locationUpdateComplete(location: nil)
            }
            return
        }
        
        if authStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
}

extension LocationProvider: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationUpdateComplete(location: locations.last)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if isAuthorized {
            locationManager.startUpdatingLocation()
        } else {
            locationUpdateComplete(location: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationUpdateComplete(location: nil)
    }
}
