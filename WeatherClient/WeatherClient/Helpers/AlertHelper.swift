//
//  AlertHelper.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

class AlertHelper {
    class func showAlert(title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "alertOkButtonTitle".localized(), style: .default, handler: nil))
        UIApplication.shared.topViewController()?.present(vc, animated: true, completion: nil)
    }
}
