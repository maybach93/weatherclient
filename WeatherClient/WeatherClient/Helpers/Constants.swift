//
//  Constants.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

enum Constants {
    enum SegueIdentifiers {
        static let authSuccessViewController = "AuthSuccessViewControllerSegue"
    }
    
    enum InputFieldsConstants {
        static let minPasswordLength = 6
        static let maxPasswordLength = 20
        static let regexPassword = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$"
        static let regexEmail = "[А-Яа-яA-Z0-9a-z._%+-]+@[А-Яа-яA-Za-z0-9.-]+\\.[А-Яа-яA-Za-z]{2,}"
    }
    
    enum AuthKeys {
        static let apiToken = "APIKEY"
        static let apiUrl = "APIURL"
    }
}
