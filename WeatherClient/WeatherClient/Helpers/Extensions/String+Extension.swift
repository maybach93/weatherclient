//
//  String+Extension.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 14.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

extension String {
    
    func localized() -> String {
        return NSLocalizedString(self, comment: self)
    }
}
