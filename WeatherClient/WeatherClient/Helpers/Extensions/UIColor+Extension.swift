//
//  UIColor+CustomColors.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

extension UIColor {
    class var authRedColor: UIColor {
        return UIColor(red: 255 / 255, green: 59 / 255, blue: 48 / 255, alpha: 1.0)
    }
    
    class var authGrayTextColor: UIColor {
        return UIColor(red: 121 / 255, green: 121 / 255, blue: 121 / 255, alpha: 1.0)
    }
    
    class var authBlackTextColor: UIColor {
        return UIColor(red: 51 / 255, green: 51 / 255, blue: 51 / 255, alpha: 1.0)
    }
    
    class var separatorColor: UIColor {
        return UIColor(red: 235 / 255, green: 235 / 255, blue: 235 / 255, alpha: 1.0)
    }
}
