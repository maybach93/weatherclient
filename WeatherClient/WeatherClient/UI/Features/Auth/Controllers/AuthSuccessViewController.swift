//
//  AuthSuccessViewController.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

class AuthSuccessViewController: UIViewController {
    
    //MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        provideWeatherForecast()
    }
    
    //MARK: - Private methods
    
    private func provideWeatherForecast() {
        LocationProvider.shared.requestCurrentLocation { [weak self] (location) in
            if let location = location {
                WeatherAPIManager.shared.weatherForecast(coordinates: (latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), { (result) in
                    switch result {
                    case let .success(data):
                        self?.show(weatherForecast: data)
                    case let .failure(error):
                        AlertHelper.showAlert(title: "alertErrorTitle".localized(), message: error.message)
                    }
                })
            } else {
                AlertHelper.showAlert(title: "alertErrorTitle".localized(), message: "alertFailedProvideLocation".localized())
            }
        }
    }
    
    private func show(weatherForecast: WeatherForecastParameters) {
        let title = String.init(format: "alertWeatherForecastTitle".localized(), weatherForecast.name)
        let message = String.init(format: "alertWeatherForecastMessage".localized(), weatherForecast.main.temperature, weatherForecast.main.minimalTemperature, weatherForecast.main.maximumTemperature)
        AlertHelper.showAlert(title: title, message: message)
    }
}
