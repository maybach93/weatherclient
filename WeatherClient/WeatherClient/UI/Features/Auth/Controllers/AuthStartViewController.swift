//
//  AuthStartViewController.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

class AuthStartViewController: UIViewController {

    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
    }
    
    //MARK: - Configuration
    
    private func configureNavigationBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
