
//
//  AuthViewController.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet weak var authContainerView: UIView!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var emailInputView: AuthEmailInputView!
    @IBOutlet weak var passwordInputView: AuthPasswordInputView!
    @IBOutlet weak var authContainerBottomConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    
    var authModel: AuthModel = AuthModel()
    var defaultAuthContainerBottomHeight: CGFloat {
        get {
            return view.bounds.height / 2 - authContainerView.bounds.height / 2
        }
    }
    
    var keyboardRevealHelper: KeyboardRevealHelper!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailInputView.delegate = self
        passwordInputView.delegate = self
        passwordInputView.passwordDelegate = self
        
        let defaultContentHeight: CGFloat = view.bounds.height / 2 - authContainerView.bounds.height / 2
        keyboardRevealHelper = KeyboardRevealHelper(view: view, defaultContentHeight: defaultContentHeight, contentBottomConstraint: authContainerBottomConstraint)
        configureNavigationBar()
    }
    
    //MARK: - Configuration
    
    private func configureNavigationBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    //MARK: - Actions
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        view.endEditing(true)
        processLoginIfPossible()
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        showNotImplementedAlert()
    }
    
    //MARK: - Private methods
    
    fileprivate func showNotImplementedAlert() {
        AlertHelper.showAlert(title: "alertErrorTitle".localized(), message: "alertNotImplementedYet".localized())
    }
    
    fileprivate func processLoginIfPossible() {
        guard authModel.isFulfilled else {
            if authModel.email == nil {
                emailInputView.isIncorrectInput = true
            }
            if authModel.password == nil {
                passwordInputView.isIncorrectInput = true
            }
            return
        }
        performSegue(withIdentifier: Constants.SegueIdentifiers.authSuccessViewController, sender: nil)
    }
}

extension AuthViewController: AuthInputViewDelegate {
    func didEndEditing(in inputView: AuthInputView, inputText: String) {
        switch inputView {
        case emailInputView:
            if AuthInputValidateUtility.isValidEmail(inputText) {
                authModel.email = inputText
            } else {
                authModel.email = nil
                inputView.isIncorrectInput = true
            }
        case passwordInputView:
            if AuthInputValidateUtility.isValidPassword(inputText) {
                authModel.password = inputText
            } else {
                authModel.password = nil
                inputView.isIncorrectInput = true
            }
        default:
            break
        }
    }
}

extension AuthViewController: AuthPasswordInputViewDelegate {
    func pressedForgotButton(in passwordInputView: AuthPasswordInputView) {
        showNotImplementedAlert()
    }
}
