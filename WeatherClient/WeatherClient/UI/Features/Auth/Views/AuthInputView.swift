//
//  AuthInputView.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit
protocol AuthInputViewDelegate: class {
    func didEndEditing(in inputView: AuthInputView, inputText: String)
}

class AuthInputView: UIView {

    //MARK: - Variables
    
    weak var delegate: AuthInputViewDelegate?
    
    var isIncorrectInput: Bool = false {
        didSet {
            separatorView.backgroundColor = isIncorrectInput ? UIColor.authRedColor : UIColor.separatorColor
            titleLabel.textColor = isIncorrectInput ? UIColor.authRedColor : UIColor.authGrayTextColor
            inputTextField.textColor = isIncorrectInput ? UIColor.authRedColor : UIColor.authBlackTextColor
        }
    }
    
    //MARK: - Outlets
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
}

extension AuthInputView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        isIncorrectInput = false
        delegate?.didEndEditing(in: self, inputText: textField.text ?? "")
    }
}
