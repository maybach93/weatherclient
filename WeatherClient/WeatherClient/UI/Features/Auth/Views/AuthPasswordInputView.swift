//
//  AuthPasswordInputView.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

protocol AuthPasswordInputViewDelegate: class {
    func pressedForgotButton(in passwordInputView: AuthPasswordInputView)
}

class AuthPasswordInputView: AuthInputView {
    
    //MARK: - Variables
    
    weak var passwordDelegate: AuthPasswordInputViewDelegate?
    
    //MARK: - Outlets
    
    @IBOutlet weak var forgotButton: CustomButton!
    
    //MARK: - Actions
    
    @IBAction func forgotButtonClicked(_ sender: UIButton) {
        passwordDelegate?.pressedForgotButton(in: self)
    }
}
