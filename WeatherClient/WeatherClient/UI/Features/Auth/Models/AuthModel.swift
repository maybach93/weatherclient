//
//  AuthModel.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

struct AuthModel {
    var email: String?
    var password: String?
    
    var isFulfilled: Bool {
        get {
            return email != nil && password != nil
        }
    }
}
