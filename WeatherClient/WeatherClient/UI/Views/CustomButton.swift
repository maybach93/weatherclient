//
//  CustomButton.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 12.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    // MARK: Variables
    
    @IBInspectable dynamic var borderColor: UIColor? {
        didSet {
            updateBorder()
        }
    }
    @IBInspectable dynamic var cornerRadius: CGFloat = 0 {
        didSet {
            updateCorner()
        }
    }
    
    // MARK: Private methods

    private func updateCorner() {
        layer.cornerRadius = cornerRadius
    }
    
    private func updateBorder() {
        layer.borderWidth = 1
        layer.borderColor = borderColor?.cgColor
    }
}
