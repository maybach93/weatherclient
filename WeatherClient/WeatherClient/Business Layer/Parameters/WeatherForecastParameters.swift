//
//  WeatherForecastParameters.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

protocol WeatherForecastParameters {
    var weather: WeatherParameters { get set }
    var main: WeatherMainParameters { get set }
    var wind: WeatherWindParameters { get set }
    var rain: WeatherRainParameters { get set }
    var clouds: WeatherCloudsParameters { get set }
    var name: String { get set }
}

protocol WeatherParameters {
    var id: Int { get set }
    var main: String { get set }
    var description: String { get set }
    var icon: String { get set }
}

protocol WeatherMainParameters {
    var temperature: Float { get set }
    var humitity: Float { get set }
    var pressure: Float { get set }
    var minimalTemperature: Float { get set }
    var maximumTemperature: Float { get set }
}

protocol WeatherWindParameters {
    var speed: Float { get set }
    var degree: Float { get set }
}

protocol WeatherRainParameters {
    var threeHours: Float? { get set }
}

protocol WeatherCloudsParameters {
    var all: Float { get set }
}

