//
//  WeatherAPIManager.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

class WeatherAPIManager {
    fileprivate let apiProtocol: WeatherAPIProtocol
    
    init(apiProtocol: WeatherAPIProtocol) {
        self.apiProtocol = apiProtocol
    }
}

extension WeatherAPIManager: WeatherAPIProtocol {
    func weatherForecast(coordinates: WeatherForecastRequest, _ closure: ((APIResult<WeatherForecastResponse, APIError>) -> Void)?) {
        self.apiProtocol.weatherForecast(coordinates: coordinates, closure)
    }
}
