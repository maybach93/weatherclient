//
//  APIRouter.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 14.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Alamofire
import Foundation

fileprivate class Config {
    static let baseURLString: String = {
        let bundle = Bundle(for: Config.self)
        let baseURL = bundle.infoDictionary![Constants.AuthKeys.apiUrl] as! String
        return baseURL
    }()
    
    static let accessToken: String = {
        let bundle = Bundle(for: Config.self)
        let baseURL = bundle.infoDictionary![Constants.AuthKeys.apiToken] as! String
        return baseURL
    }()
}

enum APIRouter {
    // MARK: User
    case weatherForecast(latitude: Double, longitude: Double)
}

extension APIRouter: URLRequestConvertible {
    public func asURLRequest() throws -> URLRequest {
        let url = try Config.baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        var parameters: [String: Any]?
        switch self {
        // MARK: User
        case let .weatherForecast(latitude, longitude):
            parameters = ["lat": latitude, "lon": longitude, "APPID": Config.accessToken, "units": "metric"]
        }
        
        urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        return urlRequest
    }
    
    var method: HTTPMethod {
        switch self {
        case .weatherForecast:
            return .get
        }
    }
    
    var path: String {
        switch self {
        // MARK: User
        case .weatherForecast:
            return "weather"
        }
    }
}
