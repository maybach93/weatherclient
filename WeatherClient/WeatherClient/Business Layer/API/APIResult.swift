//
//  APIResult.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 14.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

enum APIError: Error {
    case internetNotAvailable
    case timeout
    case serverError
    case clientError
    case unauthorized
    case slowInternetConnection
    
    var message: String {
        switch self {
        case .serverError:
            return "alertUnknownError".localized()
        case .clientError:
            return "alertUnknownError".localized()
        case .internetNotAvailable:
            return "alertInternetNotAvaible".localized()
        case .timeout:
            return "alertInternetNotAvaible".localized()
        case .unauthorized:
            return "alertUnauthorized".localized()
        case .slowInternetConnection:
            return "alertSlowInternetConnection".localized()
        }
    }
}

enum APIResult<DataType, ErrorType> {
    case success(DataType)
    case failure(ErrorType)
}
