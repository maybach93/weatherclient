//
//  WeatherForecast+APIDecodable.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation
import SwiftyJSON

extension WeatherForecastResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherForecastResponse> {
        guard let name = json["name"].string
            else {
                return .failure()
        }
        
        guard let weather = WeatherResponse.decode(json["weather"]).data,
            let main = WeatherMainResponse.decode(json["main"]).data,
            let wind = WeatherWindResponse.decode(json["wind"]).data,
            let rain = WeatherRainResponse.decode(json["rain"]).data,
            let clouds = WeatherCloudsResponse.decode(json["clouds"]).data
            else {
                return .failure()
        }

        let weatherForecast = WeatherForecastResponse(weather: weather, main: main, wind: wind, rain: rain, clouds: clouds, name: name)
        return .success(weatherForecast)
    }
}

extension WeatherResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherResponse> {
        let weatherJSON = json.array?.first
        guard let id = weatherJSON?["id"].int,
            let main = weatherJSON?["main"].string,
            let description = weatherJSON?["description"].string,
            let icon = weatherJSON?["icon"].string
            else {
                return .failure()
        }
        let weather = WeatherResponse(id: id, main: main, description: description, icon: icon)
        return .success(weather)
    }
}

extension WeatherMainResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherMainResponse> {
        guard let temperature = json["temp"].float,
            let humitity = json["humidity"].float,
            let pressure = json["pressure"].float,
            let minimalTemperature = json["temp_min"].float,
            let maximumTemperature = json["temp_max"].float
            else {
                return .failure()
        }
        let weatherMain = WeatherMainResponse(temperature: temperature, humitity: humitity, pressure: pressure, minimalTemperature: minimalTemperature, maximumTemperature: maximumTemperature)
        return .success(weatherMain)
    }
}

extension WeatherWindResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherWindResponse> {
        guard let speed = json["speed"].float,
            let degree = json["deg"].float
            else {
                return .failure()
        }
        let weatherWind = WeatherWindResponse(speed: speed, degree: degree)
        return .success(weatherWind)
    }
}

extension WeatherRainResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherRainResponse> {
        let threeHours = json["3h"].float
        let weatherRain = WeatherRainResponse(threeHours: threeHours)
        return .success(weatherRain)
    }
}

extension WeatherCloudsResponse: APIDecodable {
    public static func decode(_ json: JSON) -> APIDecoded<WeatherCloudsResponse> {
        guard let all = json["all"].float
            else {
                return .failure()
        }
        let weatherClouds = WeatherCloudsResponse(all: all)
        return .success(weatherClouds)
    }
}
