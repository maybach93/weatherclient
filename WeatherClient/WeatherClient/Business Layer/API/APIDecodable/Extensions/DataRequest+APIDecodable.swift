//
//  DataRequest+APIDecodable.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

extension DataRequest {
    public func validateResponse() -> Self {
        return validate { _, response, _ in
            switch response.statusCode {
            case 200...299:
                return .success
            case 401:
                return .failure(APIError.unauthorized)
            case 400...499:
                return .failure(APIError.clientError)
            case 500...599:
                return .failure(APIError.serverError)
            default:
                return .success
            }
            }.validate()
    }
}

extension DataRequest {
    
    @discardableResult
    func wc_response<T: APIDecodable> (queue: DispatchQueue? = nil,
                           completionHandler: @escaping (DataResponse<T>) -> Void) -> Self where T == T.DecodedType {
        return response(queue: queue,
                        responseSerializer: DataRequest.wc_responseSerializer(),
                        completionHandler: completionHandler)
    }
    
    static func wc_responseSerializer<T: APIDecodable>() -> DataResponseSerializer<T> where T == T.DecodedType {
        return DataResponseSerializer { _, _, data, error in
            if let error = error {
                if let e = error as? APIError {
                    return .failure(e)
                }
                let nsError = error as NSError
                switch nsError.code {
                case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost:
                    return .failure(APIError.internetNotAvailable)
                case NSURLErrorTimedOut:
                    return .failure(APIError.timeout)
                default:
                    return .failure(APIError.clientError)
                }
            }
            
            guard let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    return .failure(APIError.clientError)
            }
            
            let swiftyJson = JSON(json)
            if let statusCode = swiftyJson["error", "status"].int {
                switch statusCode {
                case 401:
                    return .failure(APIError.unauthorized)
                default:
                    return .failure(APIError.serverError)
                }
            }
         
            let decoded = T.decode(swiftyJson)
            switch decoded {
            case let .success(value):
                return .success(value)
            case .failure:
                return .failure(APIError.clientError)
            }
        }
    }
}
