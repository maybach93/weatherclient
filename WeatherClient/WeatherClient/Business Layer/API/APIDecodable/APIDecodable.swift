//
//  APIDecodable.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation
import SwiftyJSON

enum APIDecoded<DataType> {
    case success(DataType)
    case failure()
    
    var data: DataType? {
        switch self {
        case let .success(data):
            return data
        default:
            return nil
        }
    }
}

protocol APIDecodable {
    associatedtype DecodedType = Self
    static func decode(_ json: JSON) -> APIDecoded<DecodedType>
}

