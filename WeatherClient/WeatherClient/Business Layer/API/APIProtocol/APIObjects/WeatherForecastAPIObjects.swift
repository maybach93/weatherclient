//
//  WeatherForecastAPIObjects.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

struct WeatherForecastResponse: WeatherForecastParameters {
    var weather: WeatherParameters
    var main: WeatherMainParameters
    var wind: WeatherWindParameters
    var rain: WeatherRainParameters
    var clouds: WeatherCloudsParameters
    var name: String
}

struct WeatherResponse: WeatherParameters {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

struct WeatherMainResponse: WeatherMainParameters {
    var temperature: Float
    var humitity: Float
    var pressure: Float
    var minimalTemperature: Float
    var maximumTemperature: Float
}

struct WeatherWindResponse: WeatherWindParameters {
    var speed: Float
    var degree: Float
}

struct WeatherRainResponse: WeatherRainParameters {
    var threeHours: Float?
}

struct WeatherCloudsResponse: WeatherCloudsParameters {
    var all: Float
}
