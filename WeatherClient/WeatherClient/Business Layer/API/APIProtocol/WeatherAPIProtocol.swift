//
//  WeatherForecastAPIProtocol.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation

typealias WeatherForecastRequest = (latitude: Double, longitude: Double)

protocol WeatherAPIProtocol {
    func weatherForecast(coordinates: WeatherForecastRequest, _ closure: ((APIResult<WeatherForecastResponse, APIError>) -> Void)?)
}

