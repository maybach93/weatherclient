//
//  ApiConnector+WeatherForecastAPIProtocol.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Alamofire
import Foundation

extension APIConnector: WeatherAPIProtocol {
    func weatherForecast(coordinates: WeatherForecastRequest, _ closure: ((APIResult<WeatherForecastResponse, APIError>) -> Void)?) {
        if !APIConnector.isConnectionGood() {
            let error = APIError.slowInternetConnection
            closure?(.failure(error))
            return
        }
        
        let router = APIRouter.weatherForecast(latitude: coordinates.latitude, longitude: coordinates.longitude)
        let request = manager.request(router)
        request.validateResponse().wc_response { [unowned self] (response: DataResponse<WeatherForecastResponse>) in
            let result = self.apiResult(from: response)
            debugPrint(response)
            switch result {
            case let .success(data):
                closure?(.success(data))
            case let .failure(error):
                closure?(.failure(error))
            }
        }
        debugPrint(request)
    }
}
