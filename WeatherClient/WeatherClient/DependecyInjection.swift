//
//  DependecyInjection.swift
//  WeatherClient
//
//  Created by Vitalii Poponov on 15.10.17.
//  Copyright © 2017 Vitalii Poponov. All rights reserved.
//

import Foundation
import Swinject

fileprivate class DependencyInjection {
    static let container = Container(parent: nil) { (container) in
        
        // MARK: API
        let apiConnector = APIConnector()
        
        // MARK: Weather
        container.register(WeatherAPIProtocol.self, factory: { _ in apiConnector })
        container.register(WeatherAPIManager.self, factory: { _ in
            WeatherAPIManager(apiProtocol: container.resolve(WeatherAPIProtocol.self)!)
        }).inObjectScope(.container)
    }
}

//API

extension WeatherAPIManager {
    static var shared: WeatherAPIManager {
        return DependencyInjection.container.resolve(WeatherAPIManager.self)! }
}
